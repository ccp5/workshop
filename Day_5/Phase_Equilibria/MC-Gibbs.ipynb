{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e522c02d",
   "metadata": {},
   "source": [
    "# Gibbs ensemble simulations\n",
    "## Introduction\n",
    "In this tutorial, you will use grand canonical, multicanonical, and Gibbs ensemble Monte Carlo simulations to investigate liquid-vapour coexistence in the Lennard-Jones system.\n",
    "\n",
    "The instructions for this tutorial are contained in two notebooks:\n",
    "`MC-MUCA.ipynb` and `MC-Gibbs.ipynb`. \n",
    "You should have both of them running.\n",
    "Start in the other one `MC-MUCA.ipynb`\n",
    "and switch to this one when the instructions advise.\n",
    "\n",
    "Because you are likely to be running two programs simultaneously (one in each notebook)\n",
    "things should go faster if two processors are available for use.\n",
    "\n",
    "***\n",
    "\n",
    "The system simulated here is the same as that in the accompanying `MC-MUCA.ipynb` notebook:\n",
    "the cut-and-shifted Lennard-Jones potential with cut-off $r_{\\mathrm{cut}}= 2.5\\sigma$.\n",
    "Again, reduced units are employed, so the Lennard-Jones parameters are $\\varepsilon=1$ and $\\sigma=1$,\n",
    "and in addition Boltzmann's constant is taken to be unity $k_{\\mathrm{B}}=1$.\n",
    "The critical point is at $T_{\\mathrm{c}}=1.0779$, $\\rho_{\\mathrm{c}}=0.3190$.\n",
    "\n",
    "Just as in the accompanying notebook, \n",
    "the temperature of interest here is $T=0.95$,\n",
    "at which liquid and vapour phases may coexist with densities \n",
    "$\\rho_{\\mathrm{liq}}\\approx 0.622$ and $\\rho_{\\mathrm{vap}}\\approx 0.0665$\n",
    "(see J Vrabec, GK Kedia, G Fuchs, H Hasse, \n",
    "[*Molec Phys,* **104,** 1509 (2006)](https://doi.org/10.1080/00268970600556774)).\n",
    "At coexistence, the pressure is $P\\approx 0.045$ \n",
    "and the chemical potential $\\mu\\approx-3.14$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a60dbb00",
   "metadata": {},
   "source": [
    "## Preliminaries\n",
    "\n",
    "Start by importing some useful Python modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ce17186",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from hdf5_module import read_file\n",
    "from eos_lj import eos\n",
    "plt.style.use(['seaborn-v0_8-talk','seaborn-v0_8-darkgrid','seaborn-v0_8-colorblind'])\n",
    "plt.rc('image',cmap='viridis')\n",
    "plt.rc('legend',frameon=True,framealpha=1.0)\n",
    "plt.rc('hist',bins=100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb865c63",
   "metadata": {},
   "source": [
    "## Gibbs ensemble Monte Carlo simulations\n",
    "\n",
    "Run the following cells to make the program and start the simulation.\n",
    "It will take, perhaps, 15-20 minutes.\n",
    "While it is running, read through the program description below,\n",
    "which refers to the important program files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "faad4507",
   "metadata": {},
   "outputs": [],
   "source": [
    "!make mc_gibbs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2981e2e",
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo '&nml  /' | ./mc_gibbs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8599ae66",
   "metadata": {},
   "source": [
    "The program `mc_gibbs` simulates two systems (one liquid, one vapour) at the same time.\n",
    "Typical starting configurations are provided in the files `config_one_old.dat` and `config_two_old.dat`. \n",
    "The format of the configuration files used here is as follows:\n",
    "\n",
    "```\n",
    "   n\n",
    "   xbox ybox zbox\n",
    "   x1   y1   z1\n",
    "   x2   y2   z2\n",
    "   x3   y3   z3\n",
    "   :    :    :\n",
    "   xn   yn   zn\n",
    "```\n",
    "\n",
    "where the first line gives the number of atoms, \n",
    "the second line gives the box dimensions (in this case cubic boxes are employed) \n",
    "and the subsequent lines give the coordinates of each atom. \n",
    "You can use the Python script `dat_to_xyz.py` (outside this notebook)\n",
    "to convert these files to XYZ format, \n",
    "and use one of the supplied molecular graphics programs to view them, if you wish.  \n",
    "\n",
    "The simulations keep the total number of atoms $N_1+N_2$ and the total volume $V_1+V_2$ fixed.\n",
    "\n",
    "The program files of most interest here are `mc_gibbs.f90` and `mc_gibbs_module.f90`.\n",
    "Open both of them and take a look.\n",
    "\n",
    "The main program `mc_gibbs.f90`\n",
    "takes its run parameters from standard input using a Fortran namelist,\n",
    "making it easy to specify them through a `key=value` mechanism,\n",
    "while allowing the unspecified parameters to take default values which are built into the program.\n",
    "You should see that the default run length is 100000 steps. \n",
    "This, and the other default values, should be enough for our purposes. Each step consists of\n",
    "\n",
    "- A number of attempted single-atom moves, equal to the number of particles in each system.\n",
    "- A number, `nswap=20` by default, of attempted particle exchanges (either way) between the systems\n",
    "- An attempted volume exchange between the systems.\n",
    "\n",
    "The routines that actually perform these last two types of move are in `mc_gibbs_module.f90`. \n",
    "If you have any questions about the code, by all means ask! \n",
    "The program simply outputs the cumulative move acceptance rates at (increasing) intervals,\n",
    "to confirm that the program is running.\n",
    "Values of all the quantities of interest are stored at each step, \n",
    "and output to a file `mc_gibbs.hdf5` at the end of the run,\n",
    "for analysis in the following cells.\n",
    "\n",
    "Once you have finished looking through these files you may close them."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2a696bbb",
   "metadata": {},
   "source": [
    "Wait until the run has completed before proceeding.\n",
    "At this temperature, \n",
    "it is most likely that the two systems have stayed in their original phase \n",
    "(liquid or vapour) throughout. \n",
    "If this is the case,\n",
    "you should see that the acceptance ratios for single-particle moves are significantly different\n",
    "(for simplicity, the maximum particle displacement `dr_max` is the same in both systems).\n",
    "The acceptance ratio for volume displacements should be moderately high,\n",
    "but for particle swaps it is rather low.\n",
    "For this reason we attempt more than one swap per step.\n",
    "\n",
    "In the following,\n",
    "we must bear in mind that any averages calculated in a single system may be invalid,\n",
    "because of the possibility that the phases might have swapped.\n",
    "This is more likely at higher temperatures,\n",
    "closer to the critical temperature.\n",
    "We shall do our analysis in terms of histograms of the calculated properties,\n",
    "computed over each system separately, assuming that no swaps have happened.\n",
    "If this were not the case,\n",
    "it would be possible to combine the data into a single set and analyse it,\n",
    "or alternatively just re-do the run.\n",
    "\n",
    "The next cell opens the HDF5 file,\n",
    "and reads the simulation parameters (attributes) and datasets,\n",
    "in a way that is hopefully familiar from earlier workshops."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1bcf54ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "params, data = read_file('mc_gibbs.hdf5')\n",
    "print(params['Title'].astype(str))\n",
    "print('        nstep = {:10d}'.format(params['nstep']))\n",
    "print('Total   N1+N2 = {:10d}'.format(params['N1+N2']))\n",
    "print('Total   V1+V2 = {:10.4f}'.format(params['V1+V2']))\n",
    "T = params['T']\n",
    "print('Temperature T = {:10.4f}'.format(T))\n",
    "print('Shape of N dataset ',np.shape(data['N']))\n",
    "print('Shape of V dataset ',np.shape(data['V']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72d534bc",
   "metadata": {},
   "source": [
    "Notice that the program produces separate step-by-step values of $N$ and $V$ for each system.\n",
    "(The same is true for the other datasets as well).\n",
    "It will be instructive to look at histograms for each system separately,\n",
    "starting with the density: $\\mathcal{P}(\\rho)$.\n",
    "This should give a clue as to whether the phases swapped during the run or not.\n",
    "We also compute averages, for each system, which may match the expected values\n",
    "given at the top of this worksheet.\n",
    "We could also combine the data from the two systems before histogramming,\n",
    "which would be a better approach if some phase swapping occurred."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea88e9a5",
   "metadata": {},
   "outputs": [],
   "source": [
    "rho = data['N'] / data['V']\n",
    "rho_vap = 0.0665              # Expected vapour density from the literature\n",
    "rho_liq = 0.622               # Expected liquid density from the literature\n",
    "rho_avg = np.mean(rho,axis=0) # Two simulation averages, one in each system\n",
    "print('Average density in system 1 = {:10.4f}'.format(rho_avg[0]))\n",
    "print('Expected vapour density     = {:10.4f}'.format(rho_vap))\n",
    "print('Average density in system 2 = {:10.4f}'.format(rho_avg[1]))\n",
    "print('Expected liquid density     = {:10.4f}'.format(rho_liq))\n",
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel(r'$\\rho$')\n",
    "ax.set_ylabel(r'$\\mathcal{P}(\\rho)$')\n",
    "ax.hist(rho[:,0],density=True,label='Box 1')\n",
    "ax.hist(rho[:,1],density=True,label='Box 2')\n",
    "ax.axvline(x=rho_avg[0],c='C2',label='Box 1 average')\n",
    "ax.axvline(x=rho_avg[1],c='C3',label='Box 2 average')\n",
    "ax.axvline(x=rho_vap,ls='dashed',c='C4',label='vapour')\n",
    "ax.axvline(x=rho_liq,ls='dashed',c='C5',label='liquid')\n",
    "ax.legend()\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17afa9bb",
   "metadata": {},
   "source": [
    "All being well,\n",
    "there should be two distinct peaks around the expected values of density.\n",
    "The Gibbs ensemble should automatically adjust both systems to give coexistence. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa5d5c2c",
   "metadata": {},
   "source": [
    "## Optional: pressure calculation\n",
    "\n",
    "The coexistence pressure is not specified:\n",
    "in principle the run should calculate it,\n",
    "and we have virial datasets `data['W']` available from both boxes.\n",
    "Let us construct histograms in a similar way to the above.\n",
    "\n",
    "The following cell computes the run-averaged pressures in both systems, \n",
    "through the usual virial expression. \n",
    "Are they approximately equal to each other?\n",
    "Do they match the expected value (given at the start of this notebook)? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a56b888",
   "metadata": {},
   "outputs": [],
   "source": [
    "P = rho*T + data['W']/(3*data['V'])\n",
    "P_coex = 0.045             # Expected coexistence pressure from the literature\n",
    "P_avg  = np.mean(P,axis=0) # Two simulation averages, one in each system\n",
    "print('Average pressure in system 1  = {:10.4f}'.format(P_avg[0]))\n",
    "print('Average pressure in system 2  = {:10.4f}'.format(P_avg[1]))\n",
    "print('Expected coexistence pressure = {:10.4f}'.format(P_coex))\n",
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel(r'$P$')\n",
    "ax.set_ylabel(r'$\\mathcal{P}(P)$')\n",
    "ax.hist(P[:,0],density=True,label='Box 1')\n",
    "ax.hist(P[:,1],density=True,label='Box 2',alpha=0.8)\n",
    "ax.axvline(x=P_coex,ls='dashed',c='C2',label='coexistence')\n",
    "ax.legend()\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "929b5e50",
   "metadata": {},
   "source": [
    "The distributions are different in the vapour (narrower) and liquid (broader), \n",
    "but both should be centred on (approximately) the same value."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1fe7a31a",
   "metadata": {},
   "source": [
    "## Optional: chemical potential calculation\n",
    "\n",
    "Chemical potentials may be estimated\n",
    "through Widom test particle insertion in both systems. \n",
    "The `data['Z']` dataset actually gives an estimate of $\\exp(-\\beta\\mu)$ \n",
    "where $\\beta=1/k_{\\mathrm{B}}T$ \n",
    "and the chemical potential $\\mu$ is defined in a convention where \n",
    "the thermal de Broglie wavelength $\\Lambda=1$. \n",
    "So, this is the inverse of the activity $z=\\exp(\\beta\\mu)$.\n",
    "Once more,\n",
    "we construct histograms and compare with the expected value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5bf54319",
   "metadata": {},
   "outputs": [],
   "source": [
    "mu_coex = -3.14                     # Expected coexistence value of mu from the literature\n",
    "Z_coex  = np.exp(-mu_coex/T)        # Corresponding literature value of exp(-beta*mu)\n",
    "Z       = data['Z']\n",
    "Z_avg   = np.mean(Z,axis=0) # Two simulation averages, one in each system\n",
    "print('System 1 average     1/z = {:10.4f}'.format(Z_avg[0]))\n",
    "print('System 2 average     1/z = {:10.4f}'.format(Z_avg[1]))\n",
    "print('Expected coexistence 1/z = {:10.4f}'.format(Z_coex))\n",
    "print('System 1 estimated   mu  = {:10.4f}'.format(-T*np.log(Z_avg[0])))\n",
    "print('System 2 estimated   mu  = {:10.4f}'.format(-T*np.log(Z_avg[1])))\n",
    "print('Expected coexistence mu  = {:10.4f}'.format(mu_coex))\n",
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel(r'$Z$')\n",
    "ax.set_ylabel(r'$\\mathcal{P}(Z)$')\n",
    "ax.hist(Z[:,0],density=True,label='Box 1')\n",
    "ax.hist(Z[:,1],density=True,label='Box 2',alpha=0.5)\n",
    "ax.axvline(x=Z_coex,ls='dashed',c='C2',label='coexistence')\n",
    "ax.legend()\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2607876d",
   "metadata": {},
   "source": [
    "You might like to plot histograms for the two boxes separately\n",
    "(assuming that one box has remained liquid, and the other vapour, throughout).\n",
    "You could also experiment with the axis limits using `ax.set_xlim(...)` etc.\n",
    "\n",
    "Although the average values are (or should be) close to the expected value,\n",
    "this histogram illustrates the different character of sampling for test particle insertion,\n",
    "compared to the usual variables such as $\\rho$, $P$, as seen earlier in the worksheet.\n",
    "\n",
    "In the liquid most insertions involve large positive potential energies (overlaps) \n",
    "and hence values close to zero of the quantity being averaged,\n",
    "which is roughly $\\exp(-\\beta\\Delta U)/\\rho$.\n",
    "However, a lucky insertion in a \"hole\" with, say, 11 or 12 neighbours,\n",
    "might generate $\\beta\\Delta U\\approx -11$ or $-12$ (in reduced units), \n",
    "and values of $\\exp(-\\beta\\Delta U)$ of order $10^5$.\n",
    "\n",
    "In the vapour, most insertions will have $\\Delta U\\approx 0$,\n",
    "some will involve overlaps,\n",
    "and a small fraction will generate negative values of $\\Delta U$ due to attractive interactions\n",
    "with one or more atoms.\n",
    "The distribution of values of $\\exp(-\\beta\\Delta U)/\\rho$ will be less dramatic\n",
    "than in the liquid,\n",
    "but still quite extreme compared with those that we are used to for $\\rho$, $P$, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "741ad8be",
   "metadata": {},
   "source": [
    "This concludes the notebook, \n",
    "i.e. the Gibbs simulation part of this tutorial. \n",
    "Now you may return to the other notebook to complete the multicanonical simulation part."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e78b929a",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
