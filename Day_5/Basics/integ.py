import numpy as np
import scipy.integrate as integ

def LJs(r,σ,ε,a,b):
    return 4.0*ε*((σ/r)**12-(σ/r)**6) + a*r - b

N = 500
ρ = 0.8
E =  -1.81408043E+03
T =  1.04475480E+00
box = 8.5498797

rcut = 0.5*box

a = 24.0 * (2.0 * rcut**(-12) - rcut**(-6)) / rcut
b = 4.0 * (rcut**(-12) - rcut**(-6)) + a * rcut

rdf = np.loadtxt("RDF")
y = np.array([r*r*LJs(r,1.0,1.0,a,b)*y for r,y in zip(rdf[:,0],rdf[:,1])])
Uc = 2.0*np.pi*N*ρ*integ.simpson(y=y,x=rdf[:,0])
Ekin = 1.5*T*N
ek=Uc+Ekin
print(Uc+Ekin,E,(ek-E)/E*100.0)
