{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "fd7b7a91",
   "metadata": {},
   "source": [
    "# Monte Carlo of Lennard-Jones atoms\n",
    "In this notebook you will use the Monte Carlo method to compute the properties of a system of Lennard-Jones atoms.\n",
    "\n",
    "There is an accompanying notebook which discusses Monte Carlo sampling and Monte Carlo integration. The two notebooks are independent, so you can study them in either order, although the other one introduces more basic ideas, so we recommend you do that one first.\n",
    "\n",
    "In this directory is a third notebook, focused on constant-pressure Monte Carlo simulation of Lennard-Jones atoms, which will be the subject of a later workshop. It follows on from some of the material covered here.\n",
    "\n",
    "## Preliminaries\n",
    "Start by importing some useful Python modules and functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d413308",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from hdf5_module import read_file\n",
    "from eos_lj import eos\n",
    "plt.style.use(['seaborn-v0_8-talk','seaborn-v0_8-darkgrid','seaborn-v0_8-colorblind'])\n",
    "plt.rc('image',cmap='viridis')\n",
    "plt.rc('legend',frameon=True,framealpha=1.0)\n",
    "plt.rc('hist',bins=100)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "653e8006",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "The Lennard-Jones potential is\n",
    "\\begin{equation*}\n",
    "u_{\\mathrm{LJ}}(r) = 4\\varepsilon \\left[\n",
    "\\left( \\frac{\\sigma}{r}\\right)^{12}-\\left(\\frac{\\sigma}{r}\\right)^6 \n",
    "\\right]\n",
    "\\end{equation*}\n",
    "where $r$ is the distance between the atoms, \n",
    "$\\varepsilon$ is an energy characterizing the strength of the interaction, \n",
    "and $\\sigma$ is a length scale that characterizes the size of the atoms. \n",
    "In the following, reduced units are adopted, \n",
    "so $\\varepsilon=1$ and $\\sigma=1$, \n",
    "and in addition Boltzmann's constant is taken to be unity $k_{\\text{B}}=1$.\n",
    "\n",
    "The system of interest here consists of atoms interacting through the \n",
    "cut-and-shifted Lennard-Jones potential defined by\n",
    "\\begin{equation*}\n",
    "u(r) =\n",
    "\\begin{cases} \n",
    "u_{\\mathrm{LJ}}(r) - u_{\\mathrm{LJ}}(r_{\\text{cut}}) & r \\leq r_{\\text{cut}} \n",
    "\\\\ 0 & r> r_{\\text{cut}} \n",
    "\\end{cases}\n",
    "\\end{equation*}\n",
    "where $r_{\\text{cut}}$ is the cutoff distance. \n",
    "The choice made here is $r_{\\text{cut}}= 2.5\\sigma$.\n",
    "For this potential, \n",
    "the critical point is at $T_{\\text{c}}=1.0779$, $\\rho_{\\text{c}}=0.3190$ \n",
    "(again in reduced units).\n",
    "This exercise concentrates on the supercritical state point, $T=2.0$, $\\rho=0.5$.\n",
    "\n",
    "Thermodynamic quantities, \n",
    "and the pair distribution function $g(r)$, for this state point, \n",
    "were presented in lectures;\n",
    "in this exercise you will be comparing with those results.\n",
    "More generally, an accurate fitted equation of state for the fluid region has been developed by\n",
    "M Thol *et al*, \n",
    "[*Int J Thermophys,* **36,** 25 (2015)](https://doi.org/10.1007/s10765-014-1764-4).\n",
    "A Python program implementing their formulae is supplied in the file `eos_lj.py`. \n",
    "This can be run interactively from the command line, \n",
    "in which case it will ask for the values of $T$ and $\\rho$,\n",
    "and will print out a range of thermodynamic data for that state point.\n",
    "Instead, this worksheet has imported a function `eos` from `eos_lj`, \n",
    "which takes $T$ and $\\rho$ as arguments,\n",
    "and returns various thermodynamic quantities in a dictionary.\n",
    "The next cell prints out values for this state point,\n",
    "which should match the values given in the lecture reasonably well.\n",
    "We shall compare several of these fitted values with our simulations in this workshop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d33ce01f",
   "metadata": {},
   "outputs": [],
   "source": [
    "eos_fit = eos(temperature=2.0,density=0.5)\n",
    "for key, value in eos_fit.items():\n",
    "    print('{:20s}{:10.4f}'.format(key,value))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "57f1dd5d",
   "metadata": {},
   "source": [
    "## Monte Carlo program\n",
    "The instruction in the following cell should build the program of interest to us here, `mc_nvt`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6bef3c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "!make mc_nvt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c10548f7",
   "metadata": {},
   "source": [
    "Now execute the `mc_nvt` program, with default parameters, by running the following cell.\n",
    "The run should take roughly 10 minutes. \n",
    "While it is running, \n",
    "read through the following description of some of the features of the program,\n",
    "looking at some of the program source files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22532581",
   "metadata": {},
   "outputs": [],
   "source": [
    "!echo '&nml  /' | ./mc_nvt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd64d8f4",
   "metadata": {},
   "source": [
    "Take a look at the main program file `mc_nvt.f90`.\n",
    "This carries out a Monte Carlo simulation of $N$ atoms,\n",
    "in a fixed volume $V$,\n",
    "at specified temperature $T$.\n",
    "Simulation parameters are provided through standard input in the form of a namelist, \n",
    "a feature of Fortran \n",
    "that allows default values to be specified within the program, \n",
    "while letting you change them, using keywords, if you wish. \n",
    "So, a run using default parameters is initiated with `echo '&nml  /' | ./mc_nvt`,\n",
    "but you could choose a different maximum displacement \n",
    "by running with the command `echo '&nml dr_max=0.3 /' | ./mc_nvt`.\n",
    "\n",
    "Have a look at the overall structure of the code in `mc_nvt.f90`: \n",
    "there is a loop over steps. \n",
    "Each step consists of an attempt to move $N$ atoms; \n",
    "the appropriate routine is in the file `mc_module.f90`. \n",
    "In this case, the atoms are chosen randomly;\n",
    "you might like to consider whether this is the only valid approach.\n",
    "Also in that module is the routine for estimating the chemical potential \n",
    "by Widom test particle insertion. \n",
    "\n",
    "The LJ potential details, \n",
    "including the value of $r_{\\text{cut}}$, \n",
    "are specified in the file `potential_module.f90`. \n",
    "The potential energy and virial functions are calculated here.\n",
    "You may also be interested in the routine which calculates the configurational temperature\n",
    "from the Laplacian and squared forces. \n",
    "These key properties, just mentioned, are stored at each step,\n",
    "and output at the end of the run\n",
    "to a file `mc_nvt.hdf5`,\n",
    "which you will read in shortly, for analysis.\n",
    "(We are using HDF5 format for this,\n",
    "but there will be no need to look closely at the details).\n",
    "Standard output is just used for the crucial information \n",
    "needed to confirm that the program is running: \n",
    "at (increasing) intervals,\n",
    "the step number, CPU time consumed so far, \n",
    "and the cumulative move acceptance ratio are printed,\n",
    "as you will see in the preceding cell.\n",
    "At regular intervals `gap` steps,\n",
    "the program stores all the atomic positions,\n",
    "and these are output to the `mc_nvt.hdf5` file at the end.\n",
    "These will be used shortly to calculate the pair distribution function. \n",
    "\n",
    "Think carefully about all the calculations done in the program, \n",
    "referring back to the lecture notes. \n",
    "If there is anything that seems unclear, feel free to ask!\n",
    "\n",
    "Input and output of configurations is of less interest to us,\n",
    "so we will be brief.\n",
    "This is handled by routines within `config_io_module.f90`. \n",
    "An initial configuration of atoms was supplied in the file `config_old.dat` \n",
    "(and a backup copy is in `config_old.bak`, \n",
    "in case this gets overwritten at any stage, \n",
    "for example if you need to do equilibration followed by production runs). \n",
    "The format of the file is\n",
    "\n",
    "```\n",
    "   n\n",
    "   xbox ybox zbox\n",
    "   x1   y1   z1\n",
    "   x2   y2   z2\n",
    "   x3   y3   z3\n",
    "   :    :    :\n",
    "   xn   yn   yn\n",
    "```\n",
    "\n",
    "where the first line gives the number of atoms, \n",
    "the second line gives the box dimensions (in this exercise the box is cubic) \n",
    "and the subsequent lines give the coordinates of each atom. \n",
    "An output file `config.dat`, in the same format, is written at the end. \n",
    "(If you wish to view these files, \n",
    "first use the Python script `dat_to_xyz.py` to convert them to XYZ format, \n",
    "and use one of the supplied molecular graphics programs). \n",
    "\n",
    "When you have finished examining the program source files, you can close them.\n",
    "\n",
    "By now the run in the cell above should have finished;\n",
    "the statement `Program ends` will be printed,\n",
    "along with the CPU time taken by the run.\n",
    "Check this, and move on to the following cells.\n",
    "(Don't start executing the program a second time!)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe32ade4",
   "metadata": {},
   "source": [
    "## Reading the HDF5 file\n",
    "\n",
    "We are using a Hierarchical Data Format (HDF5) file `mc_nvt.hdf5` \n",
    "to store output from the simulation.\n",
    "HDF5 is a very flexible format for storing data, but we are only using some simple features:\n",
    "specifically `mc_nvt.hdf5` is a completely flat file containing just two kinds of object:\n",
    "*attributes* and *datasets*.\n",
    "The function `read_file` is provided to read these in.\n",
    "\n",
    "The attributes are used to store\n",
    "a few simulation parameters,\n",
    "such as the number of particles, box lengths, and temperature.\n",
    "These are key-value pairs, like a Python dictionary.\n",
    "We start by listing these, storing the most important ones in named variables\n",
    "(the number of atoms `N`, volume `V`, temperature `T`, and array of box lengths `L`).\n",
    "\n",
    "Step-by-step values are stored in the datasets, each again having a descriptive key,\n",
    "which we again list.\n",
    "The `read_file` function returns these as a dictionary of NumPy arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d456e60",
   "metadata": {},
   "outputs": [],
   "source": [
    "params, data = read_file('mc_nvt.hdf5')\n",
    "N = params['N']\n",
    "V = params['V']\n",
    "T = params['T']\n",
    "L = params['L']\n",
    "print(params['Title'].astype(str))\n",
    "print('Run steps         = {:10d}'  .format(params['nstep']))\n",
    "print('Number of atoms N = {:10d}'  .format(N))\n",
    "print('Volume          V = {:10.4f}'.format(V))\n",
    "print('Temperature     T = {:10.4f}'.format(T))\n",
    "print('Box lengths     L = {:10.4f}{:10.4f}{:10.4f}'.format(*L))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a848af06",
   "metadata": {},
   "source": [
    "The dataset `data['T']` contains the step-by-step values of the configurational temperature.\n",
    "It will be interesting to see if the average of this quantity agrees with the input temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "951b5809",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Simulation average T = {:10.4f}'.format(np.mean(data['T'])))\n",
    "print('Specified value of T = {:10.4f}'.format(T))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df2942c8",
   "metadata": {},
   "source": [
    "## Simulation Results\n",
    "\n",
    "In the following we will look at some of the other quantities.\n",
    "\n",
    "The activity is $z=\\exp(\\mu/k_{\\text{B}}T)$.\n",
    "However, the Widom test-particle insertion method gives us,\n",
    "after averaging, $\\exp(-\\mu/k_{\\text{B}}T)$,\n",
    "and this is what `data['Z']` contains.\n",
    "Let's see if it agrees with the value obtained from the fitted `eos` function.\n",
    "You can also compare with the value tabulated in the Statistical Mechanics Lecture."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13f9e984",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Simulation average Z = {:10.4f}'.format(np.mean(data['Z'])))\n",
    "print('Fitted EOS value 1/z = {:10.4f}'.format(1/eos_fit['z']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91cbe732",
   "metadata": {},
   "source": [
    "Now: something for you to do!\n",
    "The average of the virial can be used to give the simulation pressure.\n",
    "Referring to the Statistical Mechanics lecture notes, here is the formula.\n",
    "\\begin{equation*}\n",
    "P = \\frac{ N k_{\\text{B}} T}{V} + \\frac{\\langle W\\rangle}{3V}\n",
    "\\end{equation*}\n",
    "Calculate this in the cell below.\n",
    "Bear in mind that all the variables you need have already been read from the file,\n",
    "and that $k_{\\text{B}}=1$ in our reduced simulation units.\n",
    "Compare with the value quoted in the lecture notes for this state point,\n",
    "and with the value given by the approximate equation of state,\n",
    "which is already in the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7f16298",
   "metadata": {},
   "outputs": [],
   "source": [
    "P = 0 # Insert correct formula here\n",
    "print('Simulation average P = {:10.4f}'.format(P))\n",
    "print('Fitted EOS value   P = {:10.4f}'.format(eos_fit['P']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c97a79e",
   "metadata": {},
   "source": [
    "Hopefully the results are quite close. \n",
    "If we wanted, we could further analyze the step-by-step data for all these quantities\n",
    "to estimate the statistical error on the simulation averages,\n",
    "but this is not the topic of the current workshop.\n",
    "\n",
    "Instead, we shall take a closer look at the potential energy $U$ stored in `data['U']`.\n",
    "The next cell plots $U$ as a function of step.\n",
    "You might like to plot a subset of the data,\n",
    "over fewer steps,\n",
    "to get an idea of how correlated successive values are."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "df0ec8bc",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel('step')\n",
    "ax.set_ylabel(r'$U$')\n",
    "ax.plot(data['U'])\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be714934",
   "metadata": {},
   "source": [
    "The next cell plots a probability histogram $\\mathcal{P}(U)$.\n",
    "Take a close look at this,\n",
    "making sure that it looks sensible,\n",
    "especially compared with the mean value and standard deviation of the data.\n",
    "We also compare with the fitted EOS values of $U$ and $u=U/N$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c6f10a42",
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Simulation average U = {:10.4f}'.format(np.mean(data['U'])))\n",
    "print('Standard deviation U = {:10.4f}'.format(np.std(data['U'])))\n",
    "print('Simulation average u = {:10.4f}'.format(np.mean(data['U'])/N))\n",
    "print('Fitted EOS value   u = {:10.4f}'.format(eos_fit['u']))\n",
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel(r'$U$')\n",
    "ax.set_ylabel(r'$\\mathcal{P}(U)$')\n",
    "ax.hist(data['U'],density=True,label='Simulation')\n",
    "ax.axvline(N*eos_fit['u'],c='C1',label='Fitted EOS')\n",
    "ax.legend()\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a1b7cb7",
   "metadata": {},
   "source": [
    "### Heat Capacity\n",
    "Now: something more for you to do!\n",
    "\n",
    "It should be possible to use this to estimate the heat capacity at constant volume $C_V$,\n",
    "or $c_V=C_V/N$ per atom. \n",
    "Referring to the lecture notes, \n",
    "here is the formula:\n",
    "\\begin{equation*}\n",
    "c_V/k_{\\text{B}} =\n",
    "C_V/N k_{\\text{B}} = \\frac{3}{2} + \\frac{\\langle U^2\\rangle - \\langle U\\rangle^2}{N(k_{\\text{B}} T)^2} .\n",
    "\\end{equation*}\n",
    "Do this calculation in the cell below, to give $c_V$ \n",
    "(heat capacity per atom)\n",
    "as calculated in your simulation.\n",
    "Remember, that $k_{\\text{B}}=1$ in our reduced units,\n",
    "and that the other required values were already read in above.\n",
    "Compare this $c_V$ with the value quoted in the lecture for this state point,\n",
    "and also the value returned by the fitted EOS, \n",
    "which appears in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "db1f82ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "c_V = 0 # Insert correct formula here\n",
    "print('Simulation average c_V = {:10.4f}'.format(c_V))\n",
    "print('Fitted EOS value   c_V = {:10.4f}'.format(eos_fit['c_V']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08509402",
   "metadata": {},
   "source": [
    "## Pair Distribution Function\n",
    "\n",
    "Don't worry if you run out of time before tackling the remaining topics:\n",
    "the pair distribution function and ensemble reweighting.\n",
    "You can always return to this notebook in later workshops \n",
    "(you should not need to re-run the simulation,\n",
    "and after you import the necessary Python modules and functions at the top,\n",
    "you may skip to this point).\n",
    "\n",
    "The next cell re-reads the `mc_nvt.hdf5` file (in case you are returning here afresh).\n",
    "Having checked that the box is cubic,\n",
    "`L` is redefined to be a scalar rather than an array.\n",
    "\n",
    "The `data['r']` dataset contains a set of configurations (atom positions)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82dbf000",
   "metadata": {},
   "outputs": [],
   "source": [
    "params, data = read_file('mc_nvt.hdf5')\n",
    "N = params['N']\n",
    "V = params['V']\n",
    "T = params['T']\n",
    "L = params['L']\n",
    "print(params['Title'].astype(str))\n",
    "print('Run steps         = {:10d}'  .format(params['nstep']))\n",
    "print('Number of atoms N = {:10d}'  .format(N))\n",
    "print('Volume          V = {:10.4f}'.format(V))\n",
    "print('Temperature     T = {:10.4f}'.format(T))\n",
    "assert np.allclose ( L, L[0] ), print('Error: we are assuming a cubic box')\n",
    "L=L[0]\n",
    "print('Box length      L = {:10.4f}'.format(L))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61cf2948",
   "metadata": {},
   "source": [
    "Interestingly, but not unexpectedly, \n",
    "the order of indices of the `data['r']` array is reversed, compared with the Fortran order.\n",
    "Here, the step index comes first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "efc55c99",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(data['r'].shape) # Should be (nr,N,3)\n",
    "nr = data['r'].shape[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6058de98",
   "metadata": {},
   "source": [
    "We are going to calculate $g(r)$ out to half the box length (it is a cubic box).\n",
    "This will involve accumulating a histogram of pair distances.\n",
    "For this simple example we'll specify the number of bins, and this determines the bin width.\n",
    "We do the counting in a crude way, including both $ji$ and $ij$ for each pair.\n",
    "This is not necessarily the fastest approach,\n",
    "but the aim here is to show clearly what we are calculating.\n",
    "The loop over configurations should take a few seconds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e822f93b",
   "metadata": {},
   "outputs": [],
   "source": [
    "D = L/2         # Half the box length\n",
    "b = 200         # Number of bins\n",
    "h = np.zeros(b) # Histogram initialized to zero\n",
    "for r in data['r']:\n",
    "    d     = r[:,np.newaxis,:] - r[np.newaxis,:,:] # Set of all separation vectors (N,N,3)\n",
    "    d     = np.fabs(d)                            # Absolute values of vector components\n",
    "    d     = np.where(d<D,d,L-d)                   # Simple PBC in this case\n",
    "    d     = np.sqrt(np.sum(d**2,axis=-1))         # Set of all separation distances (N,N)\n",
    "    h1,rr = np.histogram(d,bins=b,range=(0.0,D))  # Separation histogram & bin edges\n",
    "    h     = h + h1                                # Accumulate histogram\n",
    "h[0] = 0      # Remove the counts arising from the diagonal of d\n",
    "h    = h / nr # Normalise by number of configurations\n",
    "h    = h / N  # Normalise by number of atoms"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63708464",
   "metadata": {},
   "source": [
    "Now stop to consider what we have calculated. \n",
    "\n",
    "For each configuration, `h1` counts the number of pair separations\n",
    "lying in the range corresponding to each histogram bin.\n",
    "We have counted each distinct pair twice (the $N\\times N$ matrix `d` is symmetric). \n",
    "This is equivalent to\n",
    "considering each atom $i$ in turn:\n",
    "each bin of `h1` counts the number of neighbours $j$ \n",
    "whose distances from $i$ fall into that bin,\n",
    "and the results are summed over all $i$.\n",
    "The double counting corresponds to $i$ being a neighbour of $j$ as well as $j$ being a neighbour of $i$.\n",
    "There are unwanted counts corresponding to the diagonal elements $i=j$, \n",
    "but these can be removed when the loop is finished.\n",
    "\n",
    "The `h` array simply sums these results for all `nr` stored configurations,\n",
    "and after normalizing by `nr` it contains the *average* number of pair separations \n",
    "lying in the range corresponding to each histogram bin.\n",
    "Again, think of this by considering each atom $i$ in turn:\n",
    "each bin of `h` contains the average number of neighbours $j$\n",
    "whose distances from $i$ fall into that bin,\n",
    "summed over all such atoms $i$.\n",
    "\n",
    "After further normalizing by `N`, each bin of `h` contains the average number of neighbours \n",
    "whose distance from *any given atom* lies in the range covered by that bin.\n",
    "\n",
    "The `rr` array gives the bin edges,\n",
    "i.e. the separation values delimiting each bin.\n",
    "It has `b+1` elements.\n",
    "We can use these values to compute the volume of the spherical shell corresponding to each bin.\n",
    "This in turn allows us to calculate what `h` would be in an ideal gas of the same density as our system.\n",
    "We proceed to do this, calling the result `h_id`: \n",
    "the ratio `h/h_id` is $g(r)$.\n",
    "Before plotting, we use the `rr` array again to compute the mid-point ($r$-value) of each bin.\n",
    "Have a look at the results.\n",
    "Does the $g(r)$ plot look sensible? \n",
    "More importantly, have you understood the way we calculate $g(r)$?\n",
    "Feel free to ask if anything is unclear!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43b12848",
   "metadata": {},
   "outputs": [],
   "source": [
    "V_shell = np.diff((4.0*np.pi/3.0)*rr**3)\n",
    "rho     = N/V\n",
    "h_id    = rho * V_shell\n",
    "g       = h / h_id\n",
    "r       = ( rr[1:] + rr[:-1] ) / 2 # Mid-points of all the bins\n",
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel(r'$r$')\n",
    "ax.set_ylabel(r'$g(r)$')\n",
    "ax.plot(r,g)\n",
    "ax.axhline(1,ls='dashed',c='C1')\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40eae7ff",
   "metadata": {},
   "source": [
    "### Using the pair distribution function\n",
    "\n",
    "This part is entirely optional, and can be skipped.\n",
    "An almost identical question appears in the MD tutorials: there is no need to answer both! \n",
    "In principle, the average potential energy may be calculated from $g(r)$ using the formula \n",
    "\\begin{equation*}\n",
    "u_{\\text{avg}} = U/N = 2\\pi\\rho\\int_0^{\\infty} u(r) \\, g(r) \\, r^2 \\, dr , \n",
    "\\end{equation*}\n",
    "and a similar formula applies to the non-ideal contribution to the pressure. \n",
    "You can calculate this using a simple SciPy/NumPy numerical integration routine. \n",
    "Don't forget that the interactions here are given by the *cut-and-shifted* LJ potential $u(r)$;\n",
    "the next cell gives a suitable function for this.\n",
    "The integral ranges over $0 \\leq r \\leq r_{\\text{cut}}$ for this potential. \n",
    "How does the result compare with the average from the MC simulation (calculated in an earlier cell),\n",
    "and with the fitted EOS?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8830ad30",
   "metadata": {},
   "outputs": [],
   "source": [
    "def u(r):\n",
    "    \"\"\"Lennard-Jones cut-and-shifted potential, r may be scalar or NumPy array.\"\"\"\n",
    "    rc  = 2.5 # Assumed cutoff distance\n",
    "    rc2 = 1.0 / rc**2\n",
    "    rc6 = rc2**3\n",
    "    uc  = 4.0*(rc6-1.0)*rc6\n",
    "    r2  = 1.0 / r**2\n",
    "    r6  = r2**3\n",
    "    ulj = 4.0*(r6-1.0)*r6\n",
    "    return np.where ( r<rc, ulj-uc, 0.0 )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e2b7d90",
   "metadata": {},
   "source": [
    "Use the above function, and the arrays `g` and `r`, \n",
    "in the next cell to calculate the desired integral by quadrature.\n",
    "For example, the function `np.trapz(f,r)` uses the trapezoidal rule,\n",
    "where `r` is an array containing the sample points,\n",
    "and `f` is an array containing values of the integrand evaluated at those points.\n",
    "(NB `np.trapz` seems likely to be renamed `np.trapezoid`, or replaced by something else,\n",
    "in NumPy 2)\n",
    "\n",
    "Again, we can compare with the fitted EOS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08d416e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Insert your code on the line below\n",
    "u_avg = 0\n",
    "eos_fit = eos(temperature=T,density=N/V)\n",
    "print('g(r) integral for u = {:10.4f}'.format(u_avg))\n",
    "print('Fitted EOS value  u = {:10.4f}'.format(eos_fit['u']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74dfea8b",
   "metadata": {},
   "source": [
    "## Ensemble Reweighting\n",
    "\n",
    "You may prefer to skip this,\n",
    "and come back to it after the third MC lecture,\n",
    "where we go into the topic in more detail.\n",
    "\n",
    "For convenience, \n",
    "the next cell re-reads the HDF5 file.\n",
    "Then the probability histogram for the potential energy, $\\mathcal{P}(U)$, is re-calculated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93ac7854",
   "metadata": {},
   "outputs": [],
   "source": [
    "params, data = read_file('mc_nvt.hdf5')\n",
    "N = params['N']\n",
    "V = params['V']\n",
    "T = params['T']\n",
    "rho = N/V"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70bc5a10",
   "metadata": {},
   "outputs": [],
   "source": [
    "P, U = np.histogram(data['U'],bins=100,density=True)\n",
    "dU   = np.diff(U)       # Get differences in bin edges for later use\n",
    "U    = (U[:-1]+U[1:])/2 # Convert bin edges into midpoint values"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "319841b8",
   "metadata": {},
   "source": [
    "This is the distribution for the simulation run at $T=2.0$.\n",
    "It is possible to use this data to estimate $\\mathcal{P}(U)$,\n",
    "and hence calculate $\\langle U\\rangle$ and other functions of potential energy,\n",
    "at nearby temperatures.\n",
    "\n",
    "The distribution function may be expressed as $\\mathcal{P}(U)\\propto \\Omega(U) \\exp(-\\beta U)$ \n",
    "where $\\Omega(U)$ is the density of states \n",
    "and $\\beta=1/k_{\\text{B}}T$. \n",
    "From the same formula at a nearby temperature $T_1$,\n",
    "it follows that the distribution $\\mathcal{P}_1(U)$ at this nearby temperature is\n",
    "\\begin{equation*}\n",
    "\\mathcal{P}_1(U) \\propto \\mathcal{P}(U) \\times \\exp\\bigl[(\\beta-\\beta_1) U\\bigr]\n",
    "\\end{equation*}\n",
    "where $\\beta_1=1/k_BT_1$.\n",
    "The function $\\mathcal{P}_1(U)$ needs to be normalized,\n",
    "after calculating it this way, so that\n",
    "$\\int \\mathcal{P}_1(U) \\, dU = 1$.\n",
    "The following cell attempts to do this for $T_1=1.8$,\n",
    "using the just-calculated $\\mathcal{P}(U)$ curve. \n",
    "It also compares the results with the fitted equation-of-state function at both temperatures,\n",
    "adding the expected values of $\\langle U\\rangle$ as vertical dashed lines in the plot.\n",
    "\n",
    "One subtlety, in general, is that the exponential function may produce very large or very small values,\n",
    "possibly leading to underflow and overflow issues.\n",
    "We should be dealing with `float64` datasets, \n",
    "and hence the variables derived from them should also be `float64`.\n",
    "Provided $\\beta$ and $\\beta_1$ are not too different from one another,\n",
    "we expect this to be sufficient,\n",
    "but we print the normalization factors just to emphasize this danger."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36a58d4c",
   "metadata": {},
   "outputs": [],
   "source": [
    "beta  = 1/T\n",
    "T1    = 1.8\n",
    "beta1 = 1/T1\n",
    "\n",
    "# Confirm that P(U) is already normalized\n",
    "norm = np.sum(P*dU) # This is the integral of P\n",
    "print('P(U)  norm = {:.5g}'.format(norm))\n",
    "\n",
    "# Compute distribution at T1, and normalize\n",
    "P1 = P * np.exp((beta-beta1)*U)\n",
    "norm = np.sum(P1*dU) # This is the integral of P1\n",
    "print('P1(U) norm = {:.5g}'.format(norm))\n",
    "P1 = P1/norm\n",
    "\n",
    "# Compute expected mean values from fitted EOS\n",
    "U_eos  = N * eos(temperature=T, density=rho)['u']\n",
    "U1_eos = N * eos(temperature=T1,density=rho)['u']\n",
    "\n",
    "# Plot distributions\n",
    "fig, ax = plt.subplots(figsize=(8,5))\n",
    "ax.set_xlabel(r'$U$')\n",
    "ax.set_ylabel(r'$\\mathcal{P}(U)$')\n",
    "line, = ax.plot(U,P,label='T={:5.2f}'.format(T))\n",
    "ax.axvline(U_eos,ls='--',c=line.get_color())\n",
    "line, = ax.plot(U,P1,label='T={:5.2f}'.format(T1))\n",
    "ax.axvline(U1_eos,ls='--',c=line.get_color())\n",
    "ax.set_ylim(bottom=0)\n",
    "ax.legend()\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b2e7ff9",
   "metadata": {},
   "source": [
    "Now, experiment with $T_1$.\n",
    "Try, say, $T_1=2.2$, $1.5$ or $2.5$.\n",
    "What is the effect on $\\mathcal{P}_1(U)$ as $T_1$ gets further from $T$?\n",
    "\n",
    "You may also like to compare with the results of a fresh MC run at temperature $T_1$ \n",
    "(taking care to equilibrate at the new temperature first)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9aedb93",
   "metadata": {},
   "source": [
    "## Further work\n",
    "There's plenty of scope to experiment with the `mc_nvt` program. \n",
    "For example, you might tinker with the parameter `dr_max`,\n",
    "observing its effect on the move acceptance ratio. \n",
    "Consider the question: how should the \"optimal\" value of `dr_max` be determined? \n",
    "What do we want to optimize?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c1dfa0a",
   "metadata": {},
   "source": [
    "This concludes the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac86c3e7",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
